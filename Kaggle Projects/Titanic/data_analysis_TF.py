# -*- coding:utf-8 -*-

"""
 @author: Zhongjie Lin
 @file: data_analysis_TF.py
 @time: 2017/12/30 15:46
 @description:
 @output:
 @details:
    After arrangement, the data now contain 12 columns with
    ID | Survived | Pclass | Sex | Age | SibSP | Parch | Fare | Cabin | Embarked | C_Aisle | C_Bins
    Embarked values: S-1, C-2, Q-3
    Sex values: male-0, female-1
    C_Aisle values: 'C'-1, 'E'-2, 'G'-3, 'D'-4, 'A'-5, 'B'-6, 'F'-7, 'T'-8
    The empty entries are replace with -1
"""
import numpy as np
import pandas as pd
from six.moves import cPickle as pickle
import re
import collections as cl
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.pylab as plb
import os
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import GradientBoostingClassifier

# Opt to output the organized training data to a xlsx file for analysis
# OPTION values: ON-1 , OFF-0
OPTION = 0

# Opt to different algorithms for analysis
"""
0 - DNN with 7 layers and each layer contains 1024 nodes
1 - Random forest implementation
2 - Gradient Boosting Machine
"""
FUNCTION_SWITCH = 2

with open('data.pickle', 'rb') as f:
    data = pickle.load(f)

train_data = data.iloc[0:800]
valid_data = data.iloc[800:891]
test_data = data.iloc[891:]
train_size = 800
valid_size = 91
test_size = 418

if OPTION == 1:
    write = pd.ExcelWriter('output.xlsx')
    train_data.to_excel(write, 'Sheet1')
    write.save()

graph = tf.Graph()


# FUNCTION DEFINITION SECTION
#
# one-hot encoding the output data
# [1,0]-Death, [0,1]-Survive
def label_encoding(label, size):
    b = np.zeros((size, 2))
    b[np.arange(size), label] = 1
    return b


def eval_accuracy(data, label):
    return np.sum(np.argmax(data, 1) == np.argmax(label, 1)) / label.shape[0] * 100


if FUNCTION_SWITCH == 0:
    # FUNCTION 0: ignore the cabin and separate it into aisle and bin, consider the '-1' as an input
    # The input size is 9
    train_data = train_data.drop('Cabin', 1)
    valid_data = valid_data.drop('Cabin', axis=1)
    test_data = test_data.drop('Cabin', axis=1)
    batch_size = 50

    with graph.as_default():
        input_dim = 9
        hidden_layer = 1024
        output_dim = 2
        train_data_dim = 750
        valid_data_dim = 141
        test_data_dim = 418
        # generate the input data
        tf_train_array = np.array(train_data.values[:, 2:], dtype=np.float32)
        tf_valid_array = np.array(valid_data.values[:, 2:], dtype=np.float32)
        tf_test_array = np.array(test_data.values[:, 2:], dtype=np.float32)
        train_label = np.array(train_data.values[:, 1], dtype=np.int32)
        valid_label = np.array(valid_data.values[:, 1], dtype=np.int32)

        tf_train_label = label_encoding(train_label, train_data_dim)
        tf_valid_label = label_encoding(valid_label, valid_data_dim)

        # generate the input placeholder
        train_dic = tf.placeholder(dtype=tf.float32, shape=[batch_size, input_dim])
        train_lab = tf.placeholder(dtype=tf.float32, shape=[batch_size, output_dim])

        # generate the weights and bias
        l1_weight = tf.Variable(tf.truncated_normal([input_dim, hidden_layer]))
        l1_bias = tf.Variable(tf.zeros(hidden_layer))

        l2_weight = tf.Variable(tf.truncated_normal([hidden_layer, hidden_layer]))
        l2_bias = tf.Variable(tf.zeros(hidden_layer))

        l3_weight = tf.Variable(tf.truncated_normal([hidden_layer, hidden_layer]))
        l3_bias = tf.Variable(tf.zeros(hidden_layer))

        l4_weight = tf.Variable(tf.truncated_normal([hidden_layer, hidden_layer]))
        l4_bias = tf.Variable(tf.zeros(hidden_layer))

        l5_weight = tf.Variable(tf.truncated_normal([hidden_layer, hidden_layer]))
        l5_bias = tf.Variable(tf.zeros(hidden_layer))

        l6_weight = tf.Variable(tf.truncated_normal([hidden_layer, hidden_layer]))
        l6_bias = tf.Variable(tf.zeros(hidden_layer))

        l7_weight = tf.Variable(tf.truncated_normal([hidden_layer, output_dim]))
        l7_bias = tf.Variable(tf.zeros(output_dim))


        # define the 2 - layer model
        def model(data):
            l1_logit = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(data, l1_weight), l1_bias)), keep_prob=1)
            l2_logit = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(l1_logit, l2_weight), l2_bias)), keep_prob=1)
            l3_logit = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(l2_logit, l3_weight), l3_bias)), keep_prob=1)
            l4_logit = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(l3_logit, l4_weight), l4_bias)), keep_prob=1)
            l5_logit = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(l4_logit, l5_weight), l5_bias)), keep_prob=1)
            l6_logit = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(l5_logit, l6_weight), l6_bias)), keep_prob=1)
            l7_logit = tf.add(tf.matmul(l6_logit, l7_weight), l7_bias) + 0.1 * tf.nn.l2_loss(
                l1_weight) + 0.1 * tf.nn.l2_loss(l2_weight) + 0.1 * tf.nn.l2_loss(l3_weight) + 0.1 * tf.nn.l2_loss(
                l4_weight) + 0.1 * tf.nn.l2_loss(l5_weight) + 0.1 * tf.nn.l2_loss(l6_weight) + 0.1 * tf.nn.l2_loss(
                l7_weight)

            return l7_logit


        logit = model(train_dic)
        loss = tf.nn.softmax_cross_entropy_with_logits(labels=train_lab, logits=logit)

        global_rate = tf.Variable(0)
        learn_rate_ini = 0.5
        learning_rate = tf.train.exponential_decay(learn_rate_ini, global_rate, 3000, 0.95)
        optimizer = tf.train.AdadeltaOptimizer(learning_rate).minimize(loss)

        train_prediction = tf.nn.softmax(model(tf_train_array))
        valid_prediction = tf.nn.softmax(model(tf_valid_array))
        test_verify = tf.nn.softmax(model(tf_test_array))

    num_steps = 200000
    iter_lab = 0
    with tf.Session(graph=graph) as sess:
        tf.global_variables_initializer().run()
        for step in range(num_steps):
            off_set = (step * batch_size) % (train_data_dim - batch_size)
            batch_data = tf_train_array[off_set:(off_set + batch_size), :]
            batch_label = tf_train_label[off_set:(off_set + batch_size), :]
            feed_dict = {train_dic: batch_data, train_lab: batch_label}

            _, l, train_predict_result, valid_predict_result, test_result = sess.run(
                [optimizer, loss, train_prediction, valid_prediction, test_verify], feed_dict=feed_dict)

            if step % 1000 == 0:
                print("The training accuracy:",
                      eval_accuracy(train_predict_result, tf_train_label))
                print("The validation accuracy:",
                      eval_accuracy(valid_predict_result, tf_valid_label))
        test_data = test_data.assign(Survived=np.argmax(test_result, axis=1))
        header = ['Id', 'Survived']
        test_data.to_csv('output.csv', columns=header, index=False)

elif FUNCTION_SWITCH == 1:
    # Random forest implementation
    train_data_dim = train_size
    valid_data_dim = valid_size
    test_data_dim = test_size
    tf_train_array = np.array(train_data.values[:, 2:], dtype=np.float32)
    tf_valid_array = np.array(valid_data.values[:, 2:], dtype=np.float32)
    tf_test_array = np.array(test_data.values[:, 2:], dtype=np.float32)
    train_label = np.array(train_data.values[:, 1], dtype=np.int32)
    valid_label = np.array(valid_data.values[:, 1], dtype=np.int32)
    tf_train_label = label_encoding(train_label, train_data_dim)
    tf_valid_label = label_encoding(valid_label, valid_data_dim)
    clf = RandomForestClassifier(n_estimators=5000)
    clf = clf.fit(tf_train_array, train_label)
    valid_result = clf.predict(tf_valid_array)
    test_result = clf.predict(tf_test_array)
    print(eval_accuracy(label_encoding(valid_result, len(valid_result)), tf_valid_label))
    test_data = test_data.assign(Survived=test_result)
    header = ['PassengerId', 'Survived']
    test_data.to_csv('output.csv', columns=header, index=False)
elif FUNCTION_SWITCH == 2:
    # Gradient Boosting machine implementation
    train_data_dim = train_size
    valid_data_dim = valid_size
    test_data_dim = test_size
    tf_train_array = np.array(train_data.values[:, 2:], dtype=np.float32)
    tf_valid_array = np.array(valid_data.values[:, 2:], dtype=np.float32)
    tf_test_array = np.array(test_data.values[:, 2:], dtype=np.float32)
    train_label = np.array(train_data.values[:, 1], dtype=np.int32)
    valid_label = np.array(valid_data.values[:, 1], dtype=np.int32)
    tf_train_label = label_encoding(train_label, train_data_dim)
    tf_valid_label = label_encoding(valid_label, valid_data_dim)
    clf = GradientBoostingClassifier(n_estimators=1000)
    clf.fit(tf_train_array, train_label)
    valid_result = clf.predict(tf_valid_array)
    test_result = clf.predict(tf_test_array)
    print(eval_accuracy(label_encoding(valid_result, len(valid_result)), tf_valid_label))
    test_data = test_data.assign(Survived=test_result)
    header = ['PassengerId', 'Survived']
    test_data.to_csv('output.csv', columns=header, index=False)
