# -*- coding:utf-8 -*-

"""
 @author: Zhongjie Lin
 @file: PreSetup.py
 @time: 2017/12/29 22:55
 @description: Read and organize the data for the Titanic-kaggle project
 @output: train_data in train_data.pickle & train_data in test_data.pickle [Pandas.DataFrame]
 @details:
    After arrangement, the data now contain 12 columns with
    ID | Survived | Pclass | Sex | Age | SibSP | Parch | Fare | Cabin | Embarked | C_Aisle | C_Bins
    Embarked values: S-1, C-2, Q-3
    Sex values: male-0, female-1
    C_Aisle values: 'C'-1, 'E'-2, 'G'-3, 'D'-4, 'A'-5, 'B'-6, 'F'-7, 'T'-8
    The empty entries are replace with -1
"""
import numpy as np
import os
from six.moves import cPickle as pickle
import re
import csv
import pandas as pd
import collections as cl


# open the file and create lists
def csv_reader():
    cwd = os.getcwd()
    files = os.listdir(cwd)
    pattern = re.compile(r'.*\.csv')
    file_list = []
    data = dict()
    for item in files:
        file_name = re.findall(pattern, item)
        if len(file_name):
            file_list.extend(file_name)
    print("Contained files are " + str(file_list))

    for item in file_list:
        file_name = item.replace('.csv', '') + '_data'
        with open(item) as f:
            raw_source = csv.reader(f)
            data[file_name] = list(raw_source)
    return data


data = csv_reader()
train_data = np.array(data['train_data'])
test_data = np.array(data['test_data'])


# print('The train data and the test data have been read from file:')
# print('     .The size of the training data is', train_data.shape)
# print('     .The size of the testing data is', test_data.shape)


# manipulate the data and delete irrelevant columns
def data_arrange(train, test):
    # after arrangement, the data_in now contain 12 columns with
    # ID | Survived | Pclass | Sex | Age | SibSP | Parch | Fare | Cabin | Embarked | C_Aisle | C_Bins
    # Embarked values: S-1, C-2, Q-3
    # Sex values: male-0, female-1
    # C_Aisle values: 'C'-1, 'E'-2, 'G'-3, 'D'-4, 'A'-5, 'B'-6, 'F'-7, 'T'-8
    # reorganize the test data
    if test.shape[1] != 12:
        emp_list = np.array(['' for i in range(len(test[:, 9]))])
        test = np.c_[test[:, 0], emp_list, test[:, 1:]]
    train = np.delete(train, (0), axis=0)
    train = np.delete(train, (8), axis=1)
    test = np.delete(test, (0), axis=0)
    test = np.delete(test, (8), axis=1)

    train = pd.DataFrame(train,
                         columns=['PassengerId', 'Survived', 'Pclass', 'Name', 'Sex', 'Age', 'SibSp', 'Parch',
                                  'Fare', 'Cabin', 'Embarked'])
    test_data = pd.DataFrame(test,
                             columns=['PassengerId', 'Survived', 'Pclass', 'Name', 'Sex', 'Age', 'SibSp', 'Parch',
                                      'Fare', 'Cabin', 'Embarked'])
    train = train.sample(frac=1)

    size_of_valid = 141
    valid_data = train.sample(n=size_of_valid, axis=0)
    train_data = train.drop(valid_data.index)
    """
     .The size of the training data is (750, 12)
     .The size of the validation data is (141, 12)
     .The size of the testing data is (418, 12)
    """
    frames = [train_data, valid_data, test_data]
    data_in = pd.concat(frames)

    """
    Feature Engineering
    1*. Pclass remains the same
    2-. From Name (Last - Title - First), we can find whether people are
        from the same family (sharing the same Last name) 
        -> extract the last names, count the appearance, create a separate 
        column to denote index
    3-. Sex remains the same -> numbers
    4-. From the title of name predict the missing age of people
        Master -> 9
        Miss -> 10 (with family); 25 (without family)
        Mr -> 25
        ** And create a separate column to indicate whether this piece of info
        is missing or not **
    5*. SibSp, Parch remains the same
    6*. Delete Ticket Number for now -> extract the ticket title and group
    7*. Fare remains the same
    8. Cabin
        ** Create a column to indicate its existence
        ** extract the Character and Number into different columns
    9-. Embarked -> numbers
    """
    # Item 2
    pattern_last_name = re.compile(r'\D+,')
    name_list = list(data_in['Name'])
    sib_val = np.array(data_in['SibSp'].values, dtype=np.int32)
    par_val = np.array(data_in['Parch'].values, dtype=np.int32)
    relatives_list = sib_val + par_val
    last_name_list = []
    fam_last_name_list = []
    for i in range(len(name_list)):
        if relatives_list[i] != 0:
            last_name = re.search(pattern_last_name, name_list[i]).group()
            last_name = last_name.replace(",", '')
            fam_last_name_list.append(last_name)
    for i in range(len(name_list)):
        last_name = re.search(pattern_last_name, name_list[i]).group()
        last_name = last_name.replace(",", '')
        last_name_list.append(last_name)

    fam_last_name_cnt = cl.Counter(fam_last_name_list)
    fam_last_name_app_list = list(fam_last_name_cnt.keys())
    index = np.arange(1, len(fam_last_name_app_list), 1)
    fam_name_dict = dict(zip(fam_last_name_app_list, index))
    last_name_column = np.ndarray(shape=[len(last_name_list), 1])
    no_family = np.ndarray(shape=[len(last_name_list), 1])
    for i in range(len(last_name_list)):
        if last_name_list[i] in fam_name_dict.keys():
            last_name_column[i] = fam_name_dict[last_name_list[i]]
            no_family[i] = 0
        else:
            last_name_column[i] = 0
            no_family[i] = 1

    data_in = data_in.assign(Last_Name=last_name_column)
    data_in = data_in.assign(NO_Family=no_family)

    # Item 3
    data_in['Sex'] = data_in['Sex'].map({'female': 1, 'male': 0})

    # Item 9
    queue = cl.Counter(list(data_in['Embarked']))
    keys = queue.keys()
    index = 1
    for item in keys:
        if item:
            data_in['Embarked'] = data_in['Embarked'].replace(item, index)
            index += 1

    # Item 4
    age_list = list(data_in['Age'])
    missing_age = np.ndarray(shape=[len(age_list), 1])
    for i in range(len(age_list)):
        if age_list[i] == '':
            missing_age[i] = 1
        else:
            missing_age[i] = 0
    data_in = data_in.assign(Missing_Age=missing_age)

    pattern_name_age = re.compile(r'M\w+\.')
    name_list = list(data_in['Name'])
    par_val = np.array(data_in['Parch'].values, dtype=np.int32)
    title_name_list = []
    for i in range(len(name_list)):
        title_name = re.findall(pattern_name_age, name_list[i])
        if title_name:
            title_name_list.extend(title_name)
        else:
            title_name_list.extend(' ')

    for i in range(len(name_list)):
        if title_name_list[i] == 'Mr.' and missing_age[i] == 1:
            # Mr missing age
            age_list[i] = 25
        elif title_name_list[i] == 'Miss.' and missing_age[i] == 1 and par_val[i] == 0:
            age_list[i] = 25
        elif title_name_list[i] == 'Miss.' and missing_age[i] == 1 and par_val[i] != 0:
            age_list[i] = 10
        elif title_name_list[i] == 'Mrs.' and missing_age[i] == 1:
            age_list[i] = 40
        elif title_name_list[i] == 'Master.' and missing_age[i] == 1:
            age_list[i] = 10
    data_in = data_in.assign(Age=age_list)

    # Item 8
    # Extract the Cabin aisle and index
    pattern_c = re.compile(r'\A\w?')
    pattern_n = re.compile(r'\d+')
    cabin = list(data_in['Cabin'])
    bins_list = []
    aisle_list = []
    for item in range(len(cabin)):
        aisle = re.findall(pattern_c, cabin[item])
        bins = re.findall(pattern_n, cabin[item])
        if aisle[0]:
            aisle_list.extend(aisle[0])
        else:
            aisle_list.extend('Z')
        if bins:
            bins_list.append(bins)
        else:
            bins_list.append([])

    indicator = 0
    bin_column = np.zeros(shape=[len(last_name_column), 1])
    for item in bins_list:
        if len(item) >= 1:
            bin_column[indicator] = item.pop()
        indicator += 1

    cabin_list = np.zeros(shape=[len(aisle_list), 1])
    for i in range(len(aisle_list)):
        if aisle_list[i] == 'Z':
            cabin_list[i] = 1

    data_in = data_in.assign(Cabin_Exist=cabin_list)
    data_in = data_in.assign(Aisle=aisle_list)
    data_in = data_in.assign(Bins=bin_column)
    data_in['Aisle'] = data_in['Aisle'].map({'C': 1, 'B': 2, 'D': 3, 'E': 4, 'A': 5, 'F': 6, 'G': 7, 'T': 8, 'Z': ''})

    data_in = data_in.drop('Name', 1)
    data_in = data_in.drop('Cabin', 1)
    data_in = data_in.replace('', -1)
    # data_in = data_in.drop('Last_Name', 1)
    return data_in


organized_data = data_arrange(train_data, test_data)


print('The training data and the test data have been organized:')

with open('data.pickle', 'wb') as f:
    pickle.dump(organized_data, f, protocol=pickle.HIGHEST_PROTOCOL)

