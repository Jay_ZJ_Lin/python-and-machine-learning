# -*- coding:utf-8 -*-

"""
 @author: Jay
 @file: numpy_basic.py
 @time: 2017/12/1 15:42
"""
import numpy as np

a1 = np.array([1, 2, 3], dtype=int)
a2 = np.zeros((3, 4))
a3 = np.ones((3, 4))
a4 = np.arange(0, 20, 1)
a5 = np.linspace(0, 20, 20)
a6 = a5.reshape((4, 5))

b1 = np.array([1, 2, 3])
b2 = np.array([1, 1, 1])

print(b1 * b2)
print(np.dot(b1, b2))

b3 = np.array([1, 2, 3, 4, 5, 6]).reshape((2, 3))
print(np.max(b3))
print(np.max(b3, axis=0))
print(np.max(b3, axis=1))
print(np.argmax(b3))
print(np.mean(b3))
print(np.cumsum(b3))
print(np.diff(b3))
print(np.nonzero(b3))  # return the coordinate of the non-zero elements
print(np.transpose(b3))
print(np.clip(b3, 2, 4))    # constrain the elements in the range.
