from PIL import Image, ImageDraw, ImageFont, ImageOps
import random
import os
"""
This scipt achieves random 4 digits screener, 
generate digits with different orientation and order
In addition, the script also generate random lines to improve 
security.
"""
# generate the dictionary for the words
low_case_c = 'abcdefghijklmnopqrstuvwxyz'
up_case_c = low_case_c.upper()
digit_c = '1234567890'
reference_table = low_case_c + up_case_c + digit_c
ref_size = len(reference_table)
# generate random 4 digits
digit_gen = []
while len(digit_gen) < 4:
    digit_gen.append(reference_table[random.randint(0, ref_size - 1)])

# generate the background
img = Image.new('RGB', (200, 50), (0, 0, 0))
img_draw = ImageDraw.Draw(img)
# generate the position of the digits
position = [(20, 0), (70, 0), (110, 0), (150, 0)]

for var in range(4):
    # generate the background for each digit
    digit_fig = Image.new('RGB', (50, 50), (0, 0, 0))
    font = ImageFont.truetype('arial.ttf', random.randint(40, 50))
    draw = ImageDraw.Draw(digit_fig)
    r_c = random.randint(0, 255)
    g_c = random.randint(0, 255)
    b_c = random.randint(0, 255)
    draw.text((0, 0), str(digit_gen[var]),
              font=font, fill=(r_c, g_c, b_c, 100))
    # rotate the text
    digit_fig = digit_fig.rotate(random.randint(-20, 20), expand=1)
    # paste to the background on each position
    img.paste(digit_fig, position[var])

# draw random lines
for var in range(random.randint(2, 5)):
    x01 = 0
    y01 = random.randint(0, 50)
    x11 = 200
    y11 = random.randint(0, 50)
    r_c = random.randint(0, 255)
    g_c = random.randint(0, 255)
    b_c = random.randint(0, 255)
    img_draw.line([x01, y01, x11, y11], fill=(r_c, g_c, b_c, 100), width=1)

    x02 = random.randint(0, 200)
    y02 = 0
    x12 = random.randint(0, 200)
    y12 = 50
    r_c = random.randint(0, 255)
    g_c = random.randint(0, 255)
    b_c = random.randint(0, 255)
    img_draw.line([x02, y02, x12, y12], fill=(r_c, g_c, b_c, 100), width=1)
os.remove('output.png')
img.save('output.png', 'png')
# reveal the results
print(digit_gen)
