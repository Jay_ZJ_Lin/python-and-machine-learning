class word_count:
    #"""
    # This class help to analyze text files to output the word count
    #
    # property names
    word_count = 0
    character_count = 0
    character_count_without_blank = 0

    # use initialization function to read the file and count the word
    def __init__(self, name):
        file = open(name)
        # read file
        file_text = file.read()
        # eliminate blanks before and after text
        file_text = file_text.strip()
        self.word_count = file_text.count(' ') + 1
        self.character_count = len(file_text)
        self.character_count_without_blank = self.character_count - \
            file_text.count(' ')
        file.close()

    # method to print out the results
    def print_property(self):
        print(
            '''
The word count of the script is: {0};
The character count is: {1}; 
The character count without blank is: {2}
            '''.format(self.word_count, self.character_count, self.character_count_without_blank).strip())
        return

    def __del__(self):
        pass


a = word_count('script_test.txt')
a.print_property()
