# -*- coding:utf-8 -*-

"""
 @author: Jay
 @file: grapper.py
 @time: 2017/11/30 17:08
 @function:
"""

import urllib.request
import re
import tensorflow

url = "http://tieba.baidu.com/p/2166231880"
user_agent = 'Mozilla/4.0 (compatible; MSIE 5.5; Windows NT)'
headers = {'User-Agent': user_agent}

request = urllib.request.Request(url, headers=headers)
resource = urllib.request.urlopen(request)
content = resource.read().decode('utf-8')

pattern = re.compile(r'src=".*?\.jpg"')
result = re.findall(pattern, content)
img_pattern = re.compile(r'http.*?\.jpg')

i = 0
for item in result:
    match = re.search(img_pattern, item)
    urllib.request.urlretrieve(match.group(), '杉本有美{num}.jpg'.format(num=str(i)))
    i += 1
