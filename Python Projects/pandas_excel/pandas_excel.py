# -*- coding:utf-8 -*-

"""
 @author: Jay
 @file: pandas_excel.py
 @time: 2017/12/5 10:20
 @description:
 1. import file and use regular expression to find key message
 2. use pandas to export message into a excel file
 3. read and print the excel file
"""

import re
import xlsxwriter as wx
import os

# open the file with encoding utf-8
file = open('student.txt', 'r', encoding='utf-8')
# read all the content
file_txt = file.read()
# find all the content that is embraced by []
pattern = re.compile(r'\[.*?\]')
# find all the content that is separated by ,
pattern_split = re.compile(r'\,')
# find name content which is embraced by ""
pattern_name = re.compile(r'".*?"')
# find all the number content
pattern_number = re.compile(r'\d+')
# find all the content that is inside []
ori_text = re.findall(pattern, file_txt)
# create the xlsx file
os.remove('output.xlsx')
wb = wx.Workbook('output.xlsx')
# create a worksheet
wb_sheet = wb.add_worksheet('output_page')
for var in range(len(ori_text)):
    # find the name
    name = re.search(pattern_name, ori_text[var])
    name = name.group().strip('"')
    # find the score
    score = re.findall(pattern_number, ori_text[var])
    # write to the file
    wb_sheet.write(var, 0, name)
    for item in range(len(score)):
        wb_sheet.write(var, item + 1, score[item])
