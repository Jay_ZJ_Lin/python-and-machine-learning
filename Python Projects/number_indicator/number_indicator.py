from PIL import Image, ImageDraw, ImageFont
import os


def number_indicator(img_name, number):

    # '''
    # The function attachs a number to the upper right corner of the image
    # Similar to the Wechat.

    # The parameters are
    # * img_name: the string value of the image name with extension
    # * number: the value of the number that wants to display (0~9)
    # '''

    # Open the image
    img = Image.open(img_name)
    draw = ImageDraw.Draw(img)
    # Retrun the size of the image
    img_size = img.size
    # Specify the position of the red circle
    x0 = round(img_size[0] * (7 / 10))
    y0 = round(img_size[1] * (1 / 10))
    x1 = round(img_size[0] * (9 / 10))
    y1 = round(img_size[1] * (3 / 10))
    position = [(x0, y0), (x1, y1)]
    # Draw the red circle
    draw.ellipse(position, fill='red', outline='red')
    # Load the font and set the font size
    font = ImageFont.truetype('arial.ttf', 40)
    # Specify the position of the text
    tx_x = round(img_size[0] * (30 / 40))
    tx_y = round(img_size[1] * (4 / 40))
    # Draw the number
    draw.text((tx_x, tx_y), str(number), font=font, fill='white')
    # Output the image
    os.remove('output.png')
    img.save('output.png', 'png')
    return


number_indicator('icon_proj.png', 6)
