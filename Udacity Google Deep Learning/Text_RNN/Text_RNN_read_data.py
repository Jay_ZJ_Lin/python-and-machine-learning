# -*- coding:utf-8 -*-

"""
 @author: Zhongjie Lin
 @file: Text_RNN_read_data.py
 @time: 2018/1/3 11:18
 @description: Download and read the data
 @output:
 @details:
 url = 'http://mattmahoney.net/dc/text8.zip'
"""
from __future__ import print_function
import collections
import math
import numpy as np
import os
import random
import tensorflow as tf
import zipfile
from matplotlib import pylab
from six.moves import range
from six.moves.urllib.request import urlretrieve
from sklearn.manifold import TSNE

url = 'http://mattmahoney.net/dc/'

METHOD_SWTICH = 1


def maybe_download(filename, expected_bytes):
    """Download a file if not present, and make sure it's the right size."""
    if not os.path.exists(filename):
        filename, _ = urlretrieve(url + filename, filename)
    statinfo = os.stat(filename)
    if statinfo.st_size == expected_bytes:
        print('Found and verified %s' % filename)
    else:
        print(statinfo.st_size)
        raise Exception(
            'Failed to verify ' + filename + '. Can you get to it with a browser?')
    return filename


filename = maybe_download('text8.zip', 31344016)


def read_data(filename):
    """Extract the first file enclosed in a zip file as a list of words"""
    with zipfile.ZipFile(filename) as f:
        data = tf.compat.as_str(f.read(f.namelist()[0])).split()
    return data


words = read_data(filename)
print('Data size %d' % len(words))

vocabulary_size = 50000


def build_dataset(words):
    """
    :param
    count: contains the most common word list [vocabulary_size] as a list pairs, the first elements count the uncommon words.
    dictionary: contains the most common words and their indexes as a dictionary
    data: transform the word sequence into numerical sequence with indexes
    :return:
    """
    count = [['UNK', -1]]
    count.extend(collections.Counter(words).most_common(vocabulary_size - 1))
    dictionary = dict()
    for word, _ in count:
        dictionary[word] = len(dictionary)
    data = list()
    unk_count = 0
    for word in words:
        if word in dictionary:
            index = dictionary[word]
        else:
            index = 0  # dictionary['UNK']
            unk_count = unk_count + 1
        data.append(index)
    count[0][1] = unk_count
    reverse_dictionary = dict(zip(dictionary.values(), dictionary.keys()))
    return data, count, dictionary, reverse_dictionary


data, count, dictionary, reverse_dictionary = build_dataset(words)
print('Most common words (+UNK)', count[:5])
print('Sample data', data[:10])
del words  # Hint to reduce memory.

data_index = 0


def generate_batch(batch_size, num_skips, skip_window):
    """
    At each time instance,
    (1) we first renew the buffer with new data
    |------|
    1, 99, 2, 5, 6, 9, 2, 10, 75, 44,...
              |------|
    1, 99, 2, 5, 6, 9, 2, 10, 75, 44,...
                       |-------|
    1, 99, 2, 5, 6, 9, 2, 10, 75, 44,...
    (2) at each buffer,
    |------|
    1, 99, 2, 5, 6, 9, 2, 10, 75, 44,...
    iterate : num_skips
    batch: [99]
    label: [1] random in [1,99,2]
    batch: [99,99]
    label: [1,2] random in [1,99,2]
    after this:
    the buffer moves 1
        |------|
    1, 99, 2, 5, 6, 9, 2, 10, 75, 44,...
    iterate : num_skips
    batch: [2]
    label: [99] random in [1,99,2]
    batch: [99,99,2,2]
    label: [1,2,99,2] random in [99,2,5]
    after this:
    the buffer moves 1
           |------|
    1, 99, 2, 5, 6, 9, 2, 10, 75, 44,...
    ...repeat for batch_size // num_skips times

    By doing this, the batch contains the target with num_skips times of repeat, the label contains the context.
    :param
    span: the window of target context.
    num_skips: the number of time instances to move buffer by 1. (How many times to reuse an input to generate a label.)
    skip_window: the radius of the window, therefore,
    we can have the structure of "--skip_window--target--skip_window--" as the window.
    buffer: the word index collection for the window of context. Since the buffer has fixed length,
    it servers as a queue, such that if the word is longer than its length, the first element will be discarded.
    :return:
    batch
    labels
    """
    global data_index
    assert batch_size % num_skips == 0
    assert num_skips <= 2 * skip_window

    if METHOD_SWTICH == 0:
        # The skip-gram model
        batch = np.ndarray(shape=(batch_size), dtype=np.int32)
        labels = np.ndarray(shape=(batch_size, 1), dtype=np.int32)
        span = 2 * skip_window + 1  # [ skip_window target skip_window ]
        buffer = collections.deque(maxlen=span)
        # renew the buffer with new words in span size
        for _ in range(span):
            buffer.append(data[data_index])
            data_index = (data_index + 1) % len(data)
        for i in range(batch_size // num_skips):
            # create a list
            target = skip_window  # target label at the center of the buffer
            targets_to_avoid = [skip_window]
            for j in range(num_skips):
                # random sample targets in the window of span
                while target in targets_to_avoid:
                    target = random.randint(0, span - 1)
                # add target to the list
                targets_to_avoid.append(target)
                # always add the target into the batch and add context in the label at each skip.
                batch[i * num_skips + j] = buffer[skip_window]
                labels[i * num_skips + j, 0] = buffer[target]
            buffer.append(data[data_index])
            data_index = (data_index + 1) % len(data)
    elif METHOD_SWTICH == 1:
        # The CBOW model
        span = 2 * skip_window + 1  # [ skip_window target skip_window ]
        batch = np.ndarray(shape=(batch_size, span - 1), dtype=np.int32)
        labels = np.ndarray(shape=(batch_size, 1), dtype=np.int32)
        buffer = collections.deque(maxlen=span)
        # initialize the buffer with new words in span size
        for _ in range(span):
            buffer.append(data[data_index])
            data_index = (data_index + 1) % len(data)
        for i in range(batch_size):
            target = skip_window
            wing = []
            for item in range(span):
                if item != target:
                    wing.append(buffer[item])
            batch[i, :] = wing
            labels[i, 0] = buffer[target]

            buffer.append(data[data_index])
            data_index = (data_index + 1) % len(data)

    return batch, labels


print('data:', [reverse_dictionary[di] for di in data[:8]])

for num_skips, skip_window in [(2, 1), (4, 2)]:
    data_index = 0
    batch, labels = generate_batch(batch_size=8, num_skips=num_skips, skip_window=skip_window)
    print('\nwith num_skips = %d and skip_window = %d:' % (num_skips, skip_window))
    # print('    batch:', [reverse_dictionary[bi] for bi in batch])
    # print('    labels:', [reverse_dictionary[li] for li in labels.reshape(8)])

batch_size = 128
embedding_size = 128  # Dimension of the embedding vector.
skip_window = 1  # How many words to consider left and right.
num_skips = 2  # How many times to reuse an input to generate a label.
# We pick a random validation set to sample nearest neighbors. here we limit the
# validation samples to the words that have a low numeric ID, which by
# construction are also the most frequent.
valid_size = 16  # Random set of words to evaluate similarity on.
valid_window = 100  # Only pick dev samples in the head of the distribution.
valid_examples = np.array(random.sample(range(valid_window), valid_size))
num_sampled = 64  # Number of negative examples to sample.

graph = tf.Graph()

with graph.as_default(), tf.device('/cpu:0'):
    # Input data.
    span = skip_window * 2
    train_dataset = tf.placeholder(tf.int32, shape=[batch_size, span])
    train_labels = tf.placeholder(tf.int32, shape=[batch_size, 1])
    valid_dataset = tf.constant(valid_examples, dtype=tf.int32)

    # Variables.
    embeddings = tf.Variable(
        tf.random_uniform([vocabulary_size, embedding_size], -1.0, 1.0))
    # Every word has its own context, therefore, we have vocabulary_size sets of weights and bias,
    # embedding_size suggests the depth of information
    softmax_weights = tf.Variable(
        tf.truncated_normal([vocabulary_size, embedding_size],
                            stddev=1.0 / math.sqrt(embedding_size)))
    softmax_biases = tf.Variable(tf.zeros([vocabulary_size]))

    # Model.
    # Look up embeddings for inputs.
    # refer the embedding of the word
    if METHOD_SWTICH == 0:
        embed = tf.nn.embedding_lookup(embeddings, train_dataset)
    elif METHOD_SWTICH == 1:
        embed = tf.nn.embedding_lookup(embeddings, train_dataset[:, 0])
        for i in range(span - 1):
            test=tf.nn.embedding_lookup(embeddings, train_dataset[:, i + 1])
            embed = tf.add(embed, tf.nn.embedding_lookup(embeddings, train_dataset[:, i + 1]))

    # Compute the softmax loss, using a sample of the negative labels each time.
    # Use If your target vocabulary(or in other words amount of classes you want to predict)
    # is really big, it is very hard to use regular softmax,
    # because you have to calculate probability for every word in dictionary.
    # By Using  sampled_softmax_loss you only take in account subset V of your vocabulary
    # to calculate your loss.
    #
    # Sampled softmax only makes sense if we sample(our V) less than vocabulary size.

    # we need to find out the weights, bias, and the embeddings. Embeddings is the vector representation of the words
    loss = tf.reduce_mean(
        tf.nn.sampled_softmax_loss(weights=softmax_weights, biases=softmax_biases, inputs=embed,
                                   labels=train_labels, num_sampled=num_sampled, num_classes=vocabulary_size))

    # Optimizer.
    # Note: The optimizer will optimize the softmax_weights AND the embeddings.
    # This is because the embeddings are defined as a variable quantity and the
    # optimizer's `minimize` method will by default modify all variable quantities
    # that contribute to the tensor it is passed.
    # See docs on `tf.train.Optimizer.minimize()` for more details.
    optimizer = tf.train.AdagradOptimizer(1.0).minimize(loss)

    # Compute the similarity between minibatch examples and all embeddings.
    # We use the cosine distance:
    norm = tf.sqrt(tf.reduce_sum(tf.square(embeddings), 1, keep_dims=True))
    normalized_embeddings = embeddings / norm
    valid_embeddings = tf.nn.embedding_lookup(
        normalized_embeddings, valid_dataset)
    # Similarity approaches to 1 means two words share the same meaning
    similarity = tf.matmul(valid_embeddings, tf.transpose(normalized_embeddings))

num_steps = 100001

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print('Initialized')
    average_loss = 0
    for step in range(num_steps):
        batch_data, batch_labels = generate_batch(
            batch_size, num_skips, skip_window)
        feed_dict = {train_dataset: batch_data, train_labels: batch_labels}
        _, l = session.run([optimizer, loss], feed_dict=feed_dict)
        average_loss += l
        if step % 2000 == 0:
            if step > 0:
                average_loss = average_loss / 2000
            # The average loss is an estimate of the loss over the last 2000 batches.
            print('Average loss at step %d: %f' % (step, average_loss))
            average_loss = 0
        # note that this is expensive (~20% slowdown if computed every 500 steps)
        if step % 10000 == 0:
            sim = similarity.eval()
            for i in range(valid_size):
                valid_word = reverse_dictionary[valid_examples[i]]
                top_k = 8  # number of nearest neighbors
                nearest = (-sim[i, :]).argsort()[1:top_k + 1]
                log = 'Nearest to %s:' % valid_word
                for k in range(top_k):
                    close_word = reverse_dictionary[nearest[k]]
                    log = '%s %s,' % (log, close_word)
                print(log)
    final_embeddings = normalized_embeddings.eval()

num_points = 400

tsne = TSNE(perplexity=30, n_components=2, init='pca', n_iter=5000, method='exact')
two_d_embeddings = tsne.fit_transform(final_embeddings[1:num_points + 1, :])


def plot(embeddings, labels):
    assert embeddings.shape[0] >= len(labels), 'More labels than embeddings'
    pylab.figure(figsize=(15, 15))  # in inches
    for i, label in enumerate(labels):
        x, y = embeddings[i, :]
        pylab.scatter(x, y)
        pylab.annotate(label, xy=(x, y), xytext=(5, 2), textcoords='offset points',
                       ha='right', va='bottom')
    pylab.show()


words = [reverse_dictionary[i] for i in range(1, num_points + 1)]
plot(two_d_embeddings, words)
