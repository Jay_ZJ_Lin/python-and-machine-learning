# -*- coding:utf-8 -*-

"""
 @author: Zhongjie Lin
 @file: reverse_seq2seq_LSTM.py
 @time: 2018/1/4 21:17
 @description: Use sequence-to-sequence LSTM to achieve the reverse word function.
 @output:
 @details:
 Use a two layer LSTM. Each layer has fixed length as max_word_size. The first layer take input and generate states.
 The states of the first layer will feed into the second layer. The second layer generate outputs
                 C    B    A
  _    _    _    |    |    |    |
 |c|->|c|->|c|->|c|->|c|->|c|->|c|
  |    |    |         |    |    |
  A    B    C         C    B    A
  @todo:
  1. increase LSTM layers;
  2. introduce dropout;
  3. introduce the states from the first layer to every cell of the second layer.
  4. Beam search.
"""
import numpy as np
import tensorflow as tf
import string
import pickle

with open('train_text.pickle', 'rb') as f:
    train_text = pickle.load(f)

train_text_list = train_text.split(' ')

del train_text

vocabulary_size = len(string.ascii_lowercase) + 1
first_letter = ord(string.ascii_lowercase[0])

batch_size = 100
max_word_size = 10


def char2id(char):
    if char in string.ascii_lowercase:
        return ord(char) - first_letter + 1
    elif char == ' ':
        return 0
    else:
        print('Unexpected character: %s' % char)
        return 0


def id2char(dictid):
    if dictid > 0:
        return chr(dictid + first_letter - 1)
    else:
        return ' '


def word2onehot(word):
    size = len(word)
    batch = np.zeros(shape=[max_word_size, vocabulary_size], dtype=np.float32)
    for item in range(max_word_size):
        if item < size:
            batch[item, char2id(word[item])] = 1.0
        else:
            batch[item, 0] = 1.0
    return batch


def logit2word(logit):
    batch = str()
    for item in logit:
        if np.argmax(item) != 0:
            batch = batch + id2char(np.argmax(item))
    return batch


def reverse_word(word):
    rev_word = str()
    if len(word) > 0:
        for item in word:
            rev_word = item + rev_word

    return rev_word


def generate_labels(word):
    rev_word = reverse_word(word)
    size = len(word)
    padding_num = max_word_size - size - 1
    batch = np.zeros(shape=[max_word_size, vocabulary_size], dtype=np.float32)
    for item in range(max_word_size):
        if item < padding_num or item >= padding_num + size:
            batch[item, 0] = 1.0
        else:
            batch[item, char2id(rev_word[item - padding_num])] = 1.0
    return batch


num_nodes = 128

graph = tf.Graph()
with graph.as_default():
    gate_num = 4
    # Parameters
    weight_i = tf.Variable(tf.truncated_normal([vocabulary_size, num_nodes * gate_num], -0.1, 0.1))
    weight_o = tf.Variable(tf.truncated_normal([num_nodes, num_nodes * gate_num], -0.1, 0.1))
    weight_b = tf.Variable(tf.zeros([1, num_nodes * gate_num]))

    second_weight_i = tf.Variable(tf.truncated_normal([vocabulary_size, num_nodes * gate_num], -0.1, 0.1))
    second_weight_o = tf.Variable(tf.truncated_normal([num_nodes, num_nodes * gate_num], -0.1, 0.1))
    second_weight_p = tf.Variable(tf.truncated_normal([num_nodes, num_nodes * gate_num], -0.1, 0.1))
    second_weight_b = tf.Variable(tf.zeros([1, num_nodes * gate_num]))

    # Variables saving state across unrollings.
    # saved_output = tf.Variable(tf.zeros([batch_size, num_nodes]), trainable=False)
    # saved_state = tf.Variable(tf.zeros([batch_size, num_nodes]), trainable=False)
    # Classifier weights and biases.
    w = tf.Variable(tf.truncated_normal([num_nodes, vocabulary_size], -0.1, 0.1))
    b = tf.Variable(tf.zeros([vocabulary_size]))


    def lstm_cell_first(i, state):
        """Create a LSTM cell. See e.g.: http://arxiv.org/pdf/1402.1128v1.pdf
        Note that in this formulation, we omit the various connections between the
        previous state and the gates."""
        # Change to one matrix operation
        col_matrix = tf.matmul(tf.expand_dims(i, 0), weight_i) + weight_b
        col_result = tf.split(col_matrix, gate_num, axis=1)

        input_gate = tf.sigmoid(col_result[0])
        forget_gate = tf.sigmoid(col_result[1])
        update = col_result[2]
        state = forget_gate * state + input_gate * tf.tanh(update)
        return state


    def lstm_cell_second(o, state):
        """Create a LSTM cell. See e.g.: http://arxiv.org/pdf/1402.1128v1.pdf
        Note that in this formulation, we omit the various connections between the
        previous state and the gates."""
        # Change to one matrix operation
        col_matrix = tf.matmul(o, second_weight_o) + second_weight_b
        col_result = tf.split(col_matrix, gate_num, axis=1)

        input_gate = tf.sigmoid(col_result[0])
        forget_gate = tf.sigmoid(col_result[1])
        update = col_result[2]
        output_gate = tf.sigmoid(col_result[3])
        state = forget_gate * state + input_gate * tf.tanh(update)
        return output_gate * tf.tanh(state), state


    data_input = tf.placeholder(dtype=tf.float32, shape=[max_word_size, vocabulary_size])
    second_data_input = tf.placeholder(dtype=tf.float32, shape=[max_word_size, vocabulary_size])


    def model(da_input):
        outputs = list()
        output = tf.Variable(tf.zeros([1, num_nodes]), trainable=False)
        state = tf.Variable(tf.zeros([1, num_nodes]), trainable=False)

        for i in range(max_word_size):
            state = lstm_cell_first(da_input[i], state)
        for j in range(max_word_size):
            output, state = lstm_cell_second(output, state)
            outputs.append(output)

        return outputs


    logits = tf.nn.xw_plus_b(tf.concat(model(data_input), axis=0), w, b)
    loss = tf.reduce_mean(
        tf.nn.softmax_cross_entropy_with_logits(
            labels=second_data_input, logits=logits))

    train_prediction = tf.nn.softmax(logits)

    # Optimizer.
    global_step = tf.Variable(0)
    learning_rate = tf.train.exponential_decay(0.5, global_step, 1000, 0.95)
    optimizer = tf.train.GradientDescentOptimizer(learning_rate)
    gradients, v = zip(*optimizer.compute_gradients(loss))
    gradients, _ = tf.clip_by_global_norm(gradients, 1.25)
    optimizer = optimizer.apply_gradients(
        zip(gradients, v), global_step=global_step)

num_steps = 70000
summary_frequency = 100

with tf.Session(graph=graph) as session:
    tf.global_variables_initializer().run()
    print('Initialized')
    mean_loss = 0
    for step in range(num_steps):
        indicator = step % len(train_text_list)
        train_batch = word2onehot(train_text_list[indicator])
        label_batch_t = generate_labels(train_text_list[indicator])

        """print('test here!!!!')
        print(logit2word(train_batch))
        print('test here!!!!')"""
        feed_dict = {
            data_input: train_batch,
            second_data_input: label_batch_t
        }
        _, l, lr, tp = session.run([optimizer, loss, learning_rate, train_prediction], feed_dict=feed_dict)
        mean_loss += l
        if step % summary_frequency == 0:
            if step > 0:
                mean_loss = mean_loss / summary_frequency
            # The mean loss is an estimate of the loss over the last few batches.
            print(
                'Average loss at step %d: %f learning rate: %f' % (step, mean_loss, lr))
            if step % (summary_frequency * 10) == 0 and step != 0:
                # Generate some samples.
                print('=' * 80)
                print(logit2word(tp))
                print(logit2word(label_batch_t))
                print('=' * 80)
