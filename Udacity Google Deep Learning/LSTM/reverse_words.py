# -*- coding:utf-8 -*-

"""
 @author: Zhongjie Lin
 @file: reverse_words.py
 @time: 2018/1/4 20:04
 @description: Generate the input for the seq2seq LSTM
 @output:
 @details:
 
"""
import pickle

# todo- Generate input and output batches for training
with open('train_text.pickle', 'rb') as f:
    train_text = pickle.load(f)
with open('valid_text.pickle', 'rb') as f:
    valid_text = pickle.load(f)


def reverse_word(word):
    rev_word = str()
    if len(word) > 0:
        for item in word:
            rev_word = item + rev_word

    return rev_word


print(reverse_word('hello'))


def reverse_text(text):
    text_list = text.split(' ')
    rev_list = list()
    for item in text_list:
        rev_list.append(reverse_word(item))
    return ' '.join(rev_list)


print(reverse_text('test the algorithm whether it works'))

num_test = 100
print(train_text[:num_test])
print('*' * 10)
print(reverse_text(train_text[:num_test]))

with open('rev_train_text.pickle', 'wb') as f:
    pickle.dump(reverse_text(train_text), f, protocol=pickle.HIGHEST_PROTOCOL)
