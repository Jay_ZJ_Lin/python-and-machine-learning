# -*- coding:utf-8 -*-

"""
 @author: Zhongjie Lin
 @file: LSTM_read_data.py
 @time: 2018/1/3 16:22
 @description:
 @output:
 @details:
 
"""
import os
import tensorflow as tf
import zipfile
from six.moves.urllib.request import urlretrieve
import pickle

url = 'http://mattmahoney.net/dc/'

def maybe_download(filename, expected_bytes):
  """Download a file if not present, and make sure it's the right size."""
  if not os.path.exists(filename):
    filename, _ = urlretrieve(url + filename, filename)
  statinfo = os.stat(filename)
  if statinfo.st_size == expected_bytes:
    print('Found and verified %s' % filename)
  else:
    print(statinfo.st_size)
    raise Exception(
      'Failed to verify ' + filename + '. Can you get to it with a browser?')
  return filename

filename = maybe_download('text8.zip', 31344016)


def read_data(filename):
    with zipfile.ZipFile(filename) as f:
        name = f.namelist()[0]
        data = tf.compat.as_str(f.read(name))
    return data


text = read_data(filename)
print('Data size %d' % len(text))

valid_size = 1000
valid_text = text[:valid_size]
train_text = text[valid_size:]
train_size = len(train_text)
print(train_size, train_text[:64])
print(valid_size, valid_text[:64])

with open('train_text.pickle', 'wb') as f:
    pickle.dump(train_text, f, protocol=pickle.HIGHEST_PROTOCOL)
with open('valid_text.pickle', 'wb') as f:
    pickle.dump(valid_text, f, protocol=pickle.HIGHEST_PROTOCOL)
