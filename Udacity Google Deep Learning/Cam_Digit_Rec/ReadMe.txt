Proposed Idea:
-Method 1:
Consider maximum 5 digits. Use a 5*10 binary vector to represent the digits.
-Method 2:
1. Use beam-search to find out all possible numbers (e.g. with prob > 0.5)
2. Find out the location of each digit
3. organize the digits according to the location of each.

