# -*- coding:utf-8 -*-

"""
 @author: Zhongjie Lin
 @file: Sequence_digit.py
 @time: 2018/1/21 14:08
 @description:
 @output:
 @details:
 
"""
import tensorflow as tf
import numpy as np
import scipy.io as sio
import pickle
import matplotlib.pyplot as plt
import matplotlib.pylab as plb
import matplotlib.image as mpimg
import os
import Generated_images as gi

"""
The data organization section of the code. The data switch is DATA_SELECTION variable. If 
DATA_SELECTION == 0: Use the generated picture for recognition and testing
"""
DATA_SELECTION = 0

if DATA_SELECTION == 0:
    num_samples = 80000
    cwd = os.getcwd()
    img_dir = cwd + '/images/'
    files = os.listdir(img_dir)
    file_num = len(files)
    if num_samples != file_num:
        gi.generate_image(num_samples)
        print('The "data.pickle" and "label.pickle" files are ready.')
    else:
        print('The "data.pickle" and "label.pickle" files are ready.')

    with open('data.pickle', 'rb') as f:
        image_data = pickle.load(f)
    with open('label.pickle', 'rb') as f:
        label_data = pickle.load(f)
    print("The image data has the shape: ", image_data.shape)  # -1,200,60
    print("The label data has the shape: ", label_data.shape)  # -1,2


    def generate_data(train, label):
        size = label.shape[0]
        train_set = np.reshape(train, [-1, 60, 200, 1])
        label_set = np.ndarray([size, 4], dtype=np.float32)
        for i in range(size):
            num_list = np.array([int(x) for x in str(label[i, 0])])
            # num_list = np.concatenate([num_list, np.array([int(label[i, 1])])]).reshape([1, -1])
            label_set[i, :] = num_list
        return train_set, label_set


    """
    def generate_data(train, label):
        size = label.shape[0]
        dim = 4 * 10 + 5
        label_set = np.ndarray([size, 4 + 1, 10], dtype=np.float32)
        train_set = np.reshape(train, [-1, 60, 200, 1])
        for i in range(size):
            num_list = np.array([int(x) for x in str(label[i, 0])]).reshape([-1, 1])
            mis_num = 4 - int(label[i, 1])
            digit = (np.arange(0, 10) == num_list).astype(np.float32)
            digit_num = (np.arange(0, 10) == int(label[i, 1])).astype(np.float32).reshape([1, -1])
            if mis_num == 0:
                element = np.concatenate([digit, digit_num], axis=0)
            else:
                zero_pad = np.zeros(shape=[mis_num, 10])
                element = np.concatenate([digit, zero_pad, digit_num], axis=0)
            label_set[i, :, :] = element
        return train_set, label_set
    """

    train_set, train_label = generate_data(image_data, label_data)
    del image_data, label_data

"""
Define the model of the system
"""
graph = tf.Graph()

"""
def accuracy_valid(data, label):
    data = np.array(data)
    label = np.array(label)
    data_trans = np.argmax(data, axis=2)
    label_trans = np.argmax(label, axis=2)
    size = data.shape[0]
    cmp = (data_trans == label_trans).astype(np.float32)
    verf = np.sum(np.sum(cmp, axis=1)) * 100 / 45
    return verf
"""

with graph.as_default():
    patch_size = 5
    batch_size = 10

    l1_depth = 48
    l2_depth = 64
    l3_depth = 128
    l4_depth = 160
    l5_depth = 192
    l6_depth = 192
    l6_num_hidden = 384
    l7_num_hidden = 192

    image_num = train_set.shape[0]
    image_height = train_set.shape[1]
    image_width = train_set.shape[2]
    image_depth = train_set.shape[3]

    num_labels = 10
    num_digit_num = 10
    num_digit = 4

    tf_train_batch = tf.placeholder(shape=[batch_size, image_height, image_width, image_depth], dtype=tf.float32)
    tf_label_batch = tf.placeholder(shape=[batch_size, num_digit], dtype=tf.float32)

    # The 1,2,3 layers are CNN, the 4, 5 layer are DNN
    # The maxpooling is applied to 2, 3 layers
    l1_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, image_depth, l1_depth]))
    l1_bias = tf.Variable(tf.zeros(shape=[1, l1_depth]))
    l2_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l1_depth, l2_depth]))
    l2_bias = tf.Variable(tf.zeros(shape=[1, l2_depth]))
    l3_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l2_depth, l3_depth]))
    l3_bias = tf.Variable(tf.zeros(shape=[1, l3_depth]))
    l4_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l3_depth, l4_depth]))
    l4_bias = tf.Variable(tf.zeros(shape=[1, l4_depth]))
    l5_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l4_depth, l5_depth]))
    l5_bias = tf.Variable(tf.zeros(shape=[1, l5_depth]))
    l6_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l5_depth, l6_depth]))
    l6_bias = tf.Variable(tf.zeros(shape=[1, l6_depth]))
    l7_weight = tf.Variable(
        tf.truncated_normal(shape=[(image_height // 4) * (image_width // 4) * l6_depth, l6_num_hidden]))
    l7_bias = tf.Variable(tf.zeros(shape=[1, l6_num_hidden]))
    l8_weight = tf.Variable(tf.truncated_normal(shape=[l6_num_hidden, l7_num_hidden]))
    l8_bias = tf.Variable(tf.zeros(shape=[1, l7_num_hidden]))
    l9_weight = tf.Variable(tf.truncated_normal(shape=[l7_num_hidden, num_digit]))
    l9_bias = tf.Variable(tf.zeros(shape=[1, num_digit]))


    def conv_net(input, weight, bias, max_pool=0):
        conv = tf.nn.conv2d(input, weight, [1, 1, 1, 1], padding='SAME')
        if max_pool == 0:
            conv_act = tf.nn.relu(conv + bias)
        elif max_pool == 1:
            conv_pool = tf.nn.max_pool(conv, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
            conv_act = tf.nn.relu(conv_pool + bias)
        return conv_act


    def weight_l2_norm(l1_weight, l2_weight, l3_weight, l4_weight, l5_weight, l6_weight, l7_weight, weight):
        return weight * (tf.nn.l2_loss(l1_weight) + tf.nn.l2_loss(l2_weight) + tf.nn.l2_loss(l3_weight) + tf.nn.l2_loss(
            l4_weight) + tf.nn.l2_loss(l5_weight) + tf.nn.l2_loss(l6_weight) + tf.nn.l2_loss(l7_weight))

        # Define the model


    def model(data):
        conv1 = conv_net(data, l1_weight, l1_bias, max_pool=1)
        conv2 = conv_net(conv1, l2_weight, l2_bias, max_pool=1)
        conv3 = conv_net(conv2, l3_weight, l3_bias)
        conv4 = conv_net(conv3, l4_weight, l4_bias)
        conv5 = conv_net(conv4, l5_weight, l5_bias)
        conv6 = conv_net(conv5, l6_weight, l6_bias)

        shape = conv6.get_shape().as_list()
        dnn_input = tf.reshape(conv6, [shape[0], shape[1] * shape[2] * shape[3]])
        dnn1 = tf.nn.relu(tf.add(tf.matmul(dnn_input, l7_weight), l7_bias))
        dnn2 = tf.nn.relu(tf.add(tf.matmul(dnn1, l8_weight), l8_bias))
        dnn3 = tf.add(tf.matmul(dnn2, l9_weight), l9_bias)

        return dnn3


    logit = model(tf_train_batch)
    loss = tf.losses.mean_squared_error(labels=tf_label_batch, predictions=logit)
    """
    softmax_result = tf.sigmoid(logit)
    flat_output = tf.reshape(softmax_result, [batch_size, -1])
    flat_label = tf.reshape(tf_label_batch, [batch_size, -1])
    loss = tf.reduce_mean(tf.keras.backend.binary_crossentropy(target=flat_label, output=flat_output))
    """
    learning_ini = 10.0
    global_rate = tf.Variable(0)
    learning_rate = tf.train.exponential_decay(learning_ini, global_rate, 1000, 0.95)
    optimizer = tf.train.AdadeltaOptimizer(learning_rate)
    gradients, v = zip(*optimizer.compute_gradients(loss))
    # gradients, _ = tf.clip_by_global_norm(gradients, 1.25)
    optimizer = optimizer.apply_gradients(zip(gradients, v), global_step=global_rate)

    # validation = tf.nn.softmax(model(tf_valid_batch))
    train_output = logit

num_steps = 10000000

with tf.Session(graph=graph) as sess:
    sess.run(tf.global_variables_initializer())
    for step in range(num_steps):
        offset = (step * batch_size) % (train_label.shape[0] - batch_size)
        data_batch = train_set[offset:(offset + batch_size), :, :, :]
        label_batch = train_label[offset:(offset + batch_size), :]
        feed_dict = {
            tf_train_batch: data_batch,
            tf_label_batch: label_batch
        }
        _, l, train_out = sess.run([optimizer, loss, train_output], feed_dict=feed_dict)
        if step % 100 == 0:
            # print('The validation accuracy is ', accuracy_valid(sf_result, label_batch))
            print('The current loss is ', l)
            print('The train output is', train_out[1, :])
