# -*- coding:utf-8 -*-

"""
 @author: Zhongjie Lin
 @file: Source_code.py
 @time: 2018/1/20 11:54
 @description:
 @output:
 @details:
 
"""
import tensorflow as tf
import numpy as np
import scipy.io as sio
import pickle
import matplotlib.pyplot as plt
import matplotlib.pylab as plb
import matplotlib.image as mpimg
import os
import Generated_images as gi

#  Select data set
# 0 - SVHN MAT: Single digit data
# 1 - NoMNIST: Single digit data
# 2 - Self_generated data: Multiple digits data

DATA_SET_SELECTION = 2

if DATA_SET_SELECTION == 0:

    raw_mat_train = sio.loadmat('train_32x32.mat')
    image_shape = raw_mat_train['X'].shape  # (32,32,3,73257)
    label_shape = raw_mat_train['y'].shape  # (73257,1)


    def generate_input_data(fun_input, fun_label):
        segment_id = 73000
        image_edge = 32
        num_labels = 10
        num_channel = 1

        train_set = np.reshape(fun_input[:, :, :segment_id], [-1, image_edge, image_edge, num_channel])
        train_label = (np.arange(0, num_labels) == np.array(fun_label[:segment_id, None])).astype(np.float32).reshape(
            -1,
            num_labels)
        valid_set = np.reshape(fun_input[:, :, segment_id:], [-1, image_edge, image_edge, num_channel])
        valid_label = (np.arange(0, num_labels) == np.array(fun_label[segment_id:, None])).astype(np.float32).reshape(
            -1,
            num_labels)

        return train_set, train_label, valid_set, valid_label


    train_set, train_label, valid_set, valid_label = generate_input_data(np.sum(raw_mat_train['X'], axis=2),
                                                                         raw_mat_train['y'])
elif DATA_SET_SELECTION == 1:
    with open('notMNIST.pickle', 'rb') as f:
        save = pickle.load(f)
        train_set = save['train_dataset']
        train_label = save['train_labels']
        valid_set = save['valid_dataset']
        valid_label = save['valid_labels']
        test_dataset = save['test_dataset']
        test_labels = save['test_labels']
        del save  # hint to help gc free up memory
        print('Training set', train_set.shape, train_label.shape)
        print('Validation set', valid_set.shape, valid_label.shape)
        print('Test set', test_dataset.shape, test_labels.shape)
    image_edge = 28
    num_channel = 1
    num_labels = 10
    train_set = np.reshape(train_set, [-1, image_edge, image_edge, num_channel])
    valid_set = np.reshape(valid_set, [-1, image_edge, image_edge, num_channel])
    train_label = (np.arange(0, num_labels) == np.array(train_label[:, None])).astype(np.float32).reshape(-1,
                                                                                                          num_labels)
    valid_label = (np.arange(0, num_labels) == np.array(valid_label[:, None])).astype(np.float32).reshape(-1,
                                                                                                          num_labels)
elif DATA_SET_SELECTION == 2:
    num_samples = 80000
    cwd = os.getcwd()
    img_dir = cwd + '/images/'
    files = os.listdir(img_dir)
    file_num = len(files)
    if num_samples != file_num:
        gi.generate_image(num_samples)
        print('The "data.pickle" and "label.pickle" files are ready.')
    else:
        print('The "data.pickle" and "label.pickle" files are ready.')

    with open('data.pickle', 'rb') as f:
        image_data = pickle.load(f)
    with open('label.pickle', 'rb') as f:
        label_data = pickle.load(f)


def accuracy_valid(data, label):
    return np.sum(np.argmax(data, 1) == np.argmax(label, 1)) * 100 / data.shape[0]


graph = tf.Graph()

with graph.as_default():
    patch_size = 5
    batch_size = 50
    verf_batch_size = 100

    l1_depth = 32
    l2_depth = 32
    l3_depth = 32
    l4_num_hidden = 64

    if DATA_SET_SELECTION == 0:
        image_edge = 32
    else:
        image_edge = 28

    num_labels = 10
    num_channel = 1

    tf_train_batch = tf.placeholder(shape=[batch_size, image_edge, image_edge, num_channel], dtype=tf.float32)
    tf_label_batch = tf.placeholder(shape=[batch_size, num_labels], dtype=tf.float32)
    tf_valid_batch = tf.constant(valid_set, dtype=tf.float32)
    tf_valid_label = tf.constant(valid_label, dtype=tf.float32)

    # The 1,2,3 layers are CNN, the 4, 5 layer are DNN
    # The maxpooling is applied to 2, 3 layers
    l2_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, num_channel, l2_depth]))
    l2_bias = tf.Variable(tf.zeros(shape=[1, l2_depth]))
    l3_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l2_depth, l3_depth]))
    l3_bias = tf.Variable(tf.zeros(shape=[1, l3_depth]))
    l4_weight = tf.Variable(tf.truncated_normal(shape=[(image_edge // 4) ** 2 * l3_depth, l4_num_hidden]))
    l4_bias = tf.Variable(tf.zeros(shape=[1, l4_num_hidden]))
    l5_weight = tf.Variable(tf.truncated_normal(shape=[l4_num_hidden, num_labels]))
    l5_bias = tf.Variable(tf.zeros(shape=[1, num_labels]))


    # Define the model
    def model(data):
        conv2 = tf.nn.conv2d(data, l2_weight, [1, 1, 1, 1], padding='SAME')
        conv2 = tf.nn.max_pool(conv2, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
        conv2 = tf.nn.relu(conv2 + l2_bias)
        conv3 = tf.nn.conv2d(conv2, l3_weight, [1, 1, 1, 1], padding='SAME')
        conv3 = tf.nn.max_pool(conv3, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
        conv3 = tf.nn.relu(conv3 + l3_bias)

        shape = conv3.get_shape().as_list()  # (batch, image_edge // 4, image_edge // 4, l3_depth)
        l0 = tf.reshape(conv3, [shape[0], shape[1] * shape[2] * shape[3]])
        l1_logit = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(l0, l4_weight), l4_bias)), keep_prob=1)
        l2_logit = tf.add(tf.matmul(l1_logit, l5_weight), l5_bias)

        return l2_logit


    logit = model(tf_train_batch)
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_label_batch, logits=logit))
    learning_ini = 1.0
    global_rate = tf.Variable(0)
    learning_rate = tf.train.exponential_decay(learning_ini, global_rate, 1000, 0.95)
    optimizer = tf.train.AdadeltaOptimizer(learning_rate).minimize(loss)

    validation = tf.nn.softmax(model(tf_valid_batch))
    train_output = tf.nn.softmax(logit)

num_steps = 10000000

with tf.Session(graph=graph) as sess:
    sess.run(tf.global_variables_initializer())
    for step in range(num_steps):
        offset = (step * batch_size) % (train_label.shape[0] - batch_size)
        data_batch = train_set[offset:(offset + batch_size), :, :, :]
        label_batch = train_label[offset:(offset + batch_size), :]
        feed_dict = {
            tf_train_batch: data_batch,
            tf_label_batch: label_batch
        }
        _, l, vd, train_out = sess.run([optimizer, loss, validation, train_output], feed_dict=feed_dict)
        if step % 100 == 0:
            print('The training accuracy is ', accuracy_valid(vd, np.array(valid_label)))
            print('The validation accuracy is ', accuracy_valid(train_out, label_batch))
            print('The current loss is ', l)
