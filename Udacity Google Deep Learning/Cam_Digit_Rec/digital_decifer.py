from PIL import Image, ImageDraw, ImageFont, ImageOps
import random
import os

"""
This script achieves random 4 digits screener, 
generate digits with different orientation and order
In addition, the script also generate random lines to improve 
security.
"""


def font_selection():
    assert os.path.isdir('font')
    cwd = os.getcwd()
    files = os.listdir(cwd + '/font')
    file_num = len(files)
    rnd_select = random.randint(0, file_num - 1)
    return cwd + '/font/' + files[rnd_select]


def digit_sequence_generator(filename, digit=1, lower_char=0, upper_char=0, digit_num=4):
    # generate the dictionary for the words
    reference_table = []
    if lower_char == 1:
        low_case_c = 'abcdefghijklmnopqrstuvwxyz'
        reference_table.extend(lower_char)
    elif upper_char == 1:
        low_case_c = 'abcdefghijklmnopqrstuvwxyz'
        up_case_c = low_case_c.upper()
        reference_table.extend(up_case_c)
    elif digit == 1:
        digit_c = '1234567890'
        reference_table.extend(digit_c)
    ref_size = len(reference_table)
    # generate random 4 digits
    digit_gen = []
    while len(digit_gen) < digit_num:
        digit_gen.append(str(reference_table[random.randint(0, ref_size - 1)]))

    # generate the background
    if digit_num == 1:
        img = Image.new('RGB', (60, 60), (0, 0, 0))
        img_draw = ImageDraw.Draw(img)
        position = [(random.randint(0, 10), random.randint(0, 2))]
    else:
        img = Image.new('RGB', (200, 60), (0, 0, 0))
        img_draw = ImageDraw.Draw(img)
        # generate the position of the digits
        position = [(20, 0), (70, 0), (110, 0), (150, 0)]

    local_font = font_selection()
    for var in range(digit_num):
        # generate the background for each digit
        digit_fig = Image.new('RGB', (50, 50), (0, 0, 0))
        try:
            font = ImageFont.truetype(local_font, random.randint(40, 50))
        except:
            print(local_font)
        draw = ImageDraw.Draw(digit_fig)
        r_c = random.randint(10, 245)
        g_c = random.randint(10, 245)
        b_c = random.randint(10, 245)
        draw.text((0, 0), digit_gen[var], font=font, fill=(r_c, g_c, b_c, 100))
        # rotate the text
        digit_fig = digit_fig.rotate(random.randint(-20, 20), expand=1)
        # paste to the background on each position
        if digit_num == 1:
            img.paste(digit_fig, position[0])
        else:
            img.paste(digit_fig, position[var])

    # draw random lines
    for var in range(random.randint(0, 2)):
        x01 = 0
        y01 = random.randint(0, 50)
        x11 = 60
        y11 = random.randint(0, 50)
        r_c = random.randint(10, 245)
        g_c = random.randint(10, 245)
        b_c = random.randint(10, 245)
        # img_draw.line([x01, y01, x11, y11], fill=(r_c, g_c, b_c, 100), width=1)

        x02 = random.randint(0, 200)
        y02 = 0
        x12 = random.randint(0, 200)
        y12 = 60
        r_c = random.randint(10, 245)
        g_c = random.randint(10, 245)
        b_c = random.randint(10, 245)
        # img_draw.line([x02, y02, x12, y12], fill=(r_c, g_c, b_c, 100), width=1)

    cwd = os.getcwd()
    img_dir = cwd + '/images/'
    img.save(img_dir + filename, 'png')
    digit_result = ''.join(digit_gen)
    digit_length = digit_num
    return digit_result, digit_length
