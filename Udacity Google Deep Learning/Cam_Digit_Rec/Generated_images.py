# -*- coding:utf-8 -*-

"""
 @author: Zhongjie Lin
 @file: Generated_images.py
 @time: 2018/1/21 12:57
 @description:
 @output:
 @details:
 
"""
import digital_decifer as dd
import pickle
import matplotlib.image as mpimg
import os
import numpy as np


def generate_image(num_samples=8, digit_num=4):
    label_dict = []
    global_hook = None
    for i in range(num_samples):
        filename = str(i) + '.png'
        digit_result, digit_length = dd.digit_sequence_generator(filename, digit_num=digit_num)
        label_dict.append([digit_result, digit_length])
        percent = int(i * 100 / num_samples)
        if percent == 100:
            print('The current progress is {} %'.format(percent))
            print("The image files have been generated.")
        if global_hook != percent:
            if percent % 5 == 0:
                print('The current progress is {} %'.format(percent))
                global_hook = percent
    with open('label.pickle', 'wb') as f:
        pickle.dump(label_dict, f, protocol=pickle.HIGHEST_PROTOCOL)


def data_size():
    cwd = os.getcwd()
    img_dir = cwd + '/images/'
    files = os.listdir(img_dir)
    img = mpimg.imread(img_dir + files[0], format='png')
    img = np.sum(img, axis=2) / img.shape[2]
    return img.shape


def read_image(batch_size, label_dict, index):
    image_shape = data_size()
    cwd = os.getcwd()
    img_dir = cwd + '/images/'
    files = os.listdir(img_dir)
    file_num = len(files)
    indicator = index % (file_num - batch_size + 1)
    file_list = files[indicator:(indicator + batch_size)]
    labels = label_dict[indicator:(indicator + batch_size)]

    image_data = np.ndarray(shape=[batch_size, image_shape[0], image_shape[1]], dtype=np.float32)
    label_data = np.ndarray(shape=[batch_size, 10], dtype=np.float32)

    for i in range(batch_size):
        num_list = np.array([int(x) for x in str(labels[i][0])]).reshape([-1, 1])
        num_logit = (np.arange(0, 10, 1) == num_list).astype(np.float32)
        label_data[i, :] = num_logit

    end_index = indicator + batch_size

    for i in range(batch_size):
        img = mpimg.imread(img_dir + file_list[i], format='png')
        img = np.sum(img, axis=2) / img.shape[2]
        image_data[i, :, :] = img

    image_data = np.reshape(image_data, newshape=[-1, image_shape[0], image_shape[1], 1])

    return image_data, label_data, end_index
