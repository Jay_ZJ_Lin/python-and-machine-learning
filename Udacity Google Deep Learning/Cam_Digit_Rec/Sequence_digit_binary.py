# -*- coding:utf-8 -*-

"""
 @author: Zhongjie Lin
 @file: Sequence_digit.py
 @time: 2018/1/21 14:08
 @description:
 @output:
 @details:
 
"""
import tensorflow as tf
import numpy as np
import pickle
import os
import Generated_images as gi
import matplotlib.image as mpimg

"""
The data organization section of the code. The data switch is DATA_SELECTION variable. If 
DATA_SELECTION == 0: Use the generated picture for recognition and testing
"""
DATA_SELECTION = 0

if DATA_SELECTION == 0:

    num_samples = 30000
    cwd = os.getcwd()
    img_dir = cwd + '/images/'
    files = os.listdir(img_dir)
    file_num = len(files)
    if num_samples != file_num:
        gi.generate_image(num_samples, digit_num=1)
        print('The image files are ready.')
    else:
        print('The image files are ready.')
    with open('label.pickle', 'rb') as f:
        label_dict = pickle.load(f)


    def data_size():
        cwd = os.getcwd()
        img_dir = cwd + '/images/'
        files = os.listdir(img_dir)
        img = mpimg.imread(img_dir + files[0], format='png')
        img = np.sum(img, axis=2) / img.shape[2]
        return img.shape


    data_shape = data_size()

"""
Define the model of the system
"""
graph = tf.Graph()


def accuracy_valid(data, label):
    data = np.array(data)
    label = np.array(label)
    data_trans = np.argmax(data, axis=1)
    label_trans = np.argmax(label, axis=1)
    size = data.shape[0]
    cmp = (data_trans == label_trans).astype(np.float32)
    verf = np.sum(cmp) * 100 / size
    return verf


with graph.as_default():
    patch_size = 5
    batch_size = 100

    l1_depth = 48
    l2_depth = 64
    l3_depth = 128
    l4_depth = 160
    l5_depth = 192
    l6_depth = 192
    l7_depth = 300
    l8_depth = 300
    l6_num_hidden = 324

    image_num = batch_size
    image_height = data_shape[0]
    image_width = data_shape[1]
    image_depth = 1

    num_labels = 10

    tf_train_batch = tf.placeholder(shape=[batch_size, image_height, image_width, image_depth], dtype=tf.float32)
    tf_label_batch = tf.placeholder(shape=[batch_size, num_labels], dtype=tf.float32)

    # The 1,2,3 layers are CNN, the 4, 5 layer are DNN
    # The maxpooling is applied to 2, 3 layers
    l1_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, image_depth, l1_depth]))
    l1_bias = tf.Variable(tf.zeros(shape=[1, l1_depth]))
    l2_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l1_depth, l2_depth]))
    l2_bias = tf.Variable(tf.zeros(shape=[1, l2_depth]))
    l3_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l2_depth, l3_depth]))
    l3_bias = tf.Variable(tf.zeros(shape=[1, l3_depth]))
    l4_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l3_depth, l4_depth]))
    l4_bias = tf.Variable(tf.zeros(shape=[1, l4_depth]))
    l5_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l4_depth, l5_depth]))
    l5_bias = tf.Variable(tf.zeros(shape=[1, l5_depth]))
    l6_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l5_depth, l6_depth]))
    l6_bias = tf.Variable(tf.zeros(shape=[1, l6_depth]))
    l7_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l6_depth, l7_depth]))
    l7_bias = tf.Variable(tf.zeros(shape=[1, l7_depth]))
    l8_weight = tf.Variable(tf.truncated_normal(shape=[patch_size, patch_size, l7_depth, l8_depth]))
    l8_bias = tf.Variable(tf.zeros(shape=[1, l8_depth]))

    l9_weight = tf.Variable(
        tf.truncated_normal(shape=[(image_height // 4) * (image_width // 4) * l8_depth, l6_num_hidden]))
    l9_bias = tf.Variable(tf.zeros(shape=[1, l6_num_hidden]))
    l10_weight = tf.Variable(tf.truncated_normal(shape=[l6_num_hidden, num_labels]))
    l10_bias = tf.Variable(tf.zeros(shape=[1, num_labels]))


    def conv_net(input, weight, bias, max_pool=0):
        conv = tf.nn.conv2d(input, weight, [1, 1, 1, 1], padding='SAME')
        if max_pool == 0:
            conv_act = tf.nn.relu(conv + bias)
        elif max_pool == 1:
            conv_pool = tf.nn.max_pool(conv, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
            conv_act = tf.nn.relu(conv_pool + bias)
        return conv_act


    def model(data):
        conv1 = conv_net(data, l1_weight, l1_bias, max_pool=1)
        conv2 = conv_net(conv1, l2_weight, l2_bias, max_pool=1)
        conv3 = conv_net(conv2, l3_weight, l3_bias)
        conv4 = conv_net(conv3, l4_weight, l4_bias)
        conv5 = conv_net(conv4, l5_weight, l5_bias)
        conv6 = conv_net(conv5, l6_weight, l6_bias)
        conv7 = conv_net(conv6, l7_weight, l7_bias)
        conv8 = conv_net(conv7, l8_weight, l8_bias)

        shape = conv8.get_shape().as_list()
        dnn_input = tf.reshape(conv8, [shape[0], shape[1] * shape[2] * shape[3]])
        dnn1 = tf.nn.relu(tf.add(tf.matmul(dnn_input, l9_weight), l9_bias))
        dnn2 = tf.add(tf.matmul(dnn1, l10_weight), l10_bias)

        l1_regularization = tf.contrib.layers.l1_regularizer(scale=0.05, scope=None)
        weights = [l1_weight, l2_weight, l3_weight, l4_weight, l5_weight, l6_weight, l7_weight, l8_weight]
        regularization_penalty = tf.contrib.layers.apply_regularization(l1_regularization, weights)
        loss = dnn2 + regularization_penalty
        return loss


    logit = model(tf_train_batch)
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_label_batch, logits=logit))

    learning_ini = 1.0
    global_rate = tf.Variable(0)
    learning_rate = tf.train.exponential_decay(learning_ini, global_rate, 1000, 0.95)
    optimizer = tf.train.AdadeltaOptimizer(learning_rate).minimize(loss)

    # validation = tf.nn.softmax(model(tf_valid_batch))
    train_output = logit

num_steps = 1000000

with tf.Session(graph=graph) as sess:
    sess.run(tf.global_variables_initializer())
    end_index = 0
    for step in range(num_steps):
        image_data, label_data, end_index = gi.read_image(batch_size, label_dict, end_index)
        data_batch = image_data
        label_batch = label_data
        feed_dict = {
            tf_train_batch: data_batch,
            tf_label_batch: label_batch
        }
        _, l, train_out = sess.run([optimizer, loss, train_output], feed_dict=feed_dict)
        if step % 100 == 0:
            print('The validation accuracy is ', accuracy_valid(train_out, label_batch))
            print('The current loss is ', l)
            print('The train output is', train_out[1, :])
            print('The label output is', label_batch[1, :])
        accuracy = accuracy_valid(train_out, label_batch)
        if accuracy >= 95:
            weight_list = [l1_weight, l2_weight, l3_weight, l4_weight, l5_weight, l6_weight, l7_weight, l8_weight,
                           l1_bias, l2_bias, l3_bias, l4_bias, l5_bias, l6_bias, l7_bias, l8_bias]
            with open('weight.pickle', 'wb') as f:
                pickle.dump(weight_list, f, protocol=pickle.HIGHEST_PROTOCOL)
            print("The Data has saved")
