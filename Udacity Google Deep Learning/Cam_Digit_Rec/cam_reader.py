# -*- coding:utf-8 -*-

"""
 @author: Zhongjie Lin
 @file: cam_reader.py
 @time: 2018/1/2 22:30
 @description: Read camera images.
 @output: frame
 @details:
 None
"""
import numpy as np
import cv2


def cam_reader(camera_id):
    cap = cv2.VideoCapture(camera_id)
    while (True):
        # Read the image
        ret, frame = cap.read()
        # Set to gray and 28*28
        gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
        #cap.set(3, 3)
        #cap.set(4, 3)
        # Show the image
        cv2.imshow('frame', gray)
        print(np.array(frame).shape)

        # Quit by press 'q'
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    cap.release()
    cv2.destroyAllWindows()


cam_reader(0)
