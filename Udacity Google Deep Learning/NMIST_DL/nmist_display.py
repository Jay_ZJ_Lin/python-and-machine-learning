# -*- coding:utf-8 -*-

"""
 @author: Jay
 @file: nmist_display.py
 @time: 2017/12/8 12:13
"""
from __future__ import print_function
import matplotlib.pyplot as plt
import pylab
import numpy as np
import os
from IPython.display import display, Image
from sklearn.linear_model import LogisticRegression
from six.moves import cPickle as pickle
import re
import math


# Problem 1
display(Image(filename="a2F6b28udHRm.png"))

# Problem 2
image_size = 28
pic_folders = os.listdir('notMNIST_large')
pattern = re.compile(r'.*\.pickle')
array = np.ndarray(shape=(image_size, image_size), dtype=float)

for pic_folder in pic_folders:
    if re.search(pattern, pic_folder):
        f = open('notMNIST_large/' + pic_folder, 'rb')
        dataset = pickle.load(f)
        array = dataset[5, :, :]
        imgplot = plt.imshow(array)
        pylab.show()
        print(pic_folder)
        f.close()


# Problem 3,4 -- ignored



# Problem 6
def Cross_Entropy(x, label):
    return -np.dot(label, np.log(x))

train_enable_size=5000
valid_enable_size=500

f = open('notMNIST.pickle', 'rb')
data_set = pickle.load(f)
overlap_count = 0
train_size = len(data_set['train_labels'])
trainset = data_set['train_dataset']
trainlab=data_set['train_labels']
valid_size = len(data_set['valid_labels'])
validset = data_set['valid_dataset']
validlab=data_set['valid_labels']
test_size = len(data_set['test_labels'])
testset = data_set['test_dataset']
testlab=data_set['test_labels']
data_skew = trainset[1, :, :].reshape(28 * 28, 1)
valid_skew = trainset[1, :, :].reshape(28 * 28, 1)
classifier = LogisticRegression()
y=trainlab[:train_enable_size]
for i in range(1, train_enable_size):
    data_skew = np.append(data_skew, trainset[i, :, :].reshape(28 * 28, 1), axis=1)
classifier.fit(data_skew.T, y)

for i in range(1,valid_enable_size):
    valid_skew = np.append(valid_skew, validset[i, :, :].reshape(28 * 28, 1), axis=1)
x = classifier.predict(valid_skew.T)
print(100-np.count_nonzero(x-validlab[:valid_enable_size])/valid_enable_size*100,end="%\n")

