# -*- coding:utf-8 -*-

"""
 @author: Jay
 @file: nmist_display.py
 @time: 2017/12/8 12:13
"""
from __future__ import print_function
import numpy as np
from six.moves import cPickle as pickle
import tensorflow as tf


# Problem 7
# Use tensorflow to achieve the learning process

pickle_file = 'notMNIST.pickle'

with open(pickle_file, 'rb') as f:
    save = pickle.load(f)
    train_dataset = save['train_dataset']
    train_labels = save['train_labels']
    valid_dataset = save['valid_dataset']
    valid_labels = save['valid_labels']
    test_dataset = save['test_dataset']
    test_labels = save['test_labels']
    del save  # hint to help gc free up memory
    print('Training set', train_dataset.shape, train_labels.shape)
    print('Validation set', valid_dataset.shape, valid_labels.shape)
    print('Test set', test_dataset.shape, test_labels.shape)

image_size = 28
num_labels = 10

# Reformat the data and labels

def reformat(dataset, labels):
    dataset = dataset.reshape((-1, image_size * image_size)).astype(np.float32)
    # Map 0 to [1.0, 0.0, 0.0 ...], 1 to [0.0, 1.0, 0.0 ...]
    labels = (np.arange(num_labels) == labels[:, None]).astype(np.float32)
    return dataset, labels


train_dataset, train_labels = reformat(train_dataset, train_labels)
valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
test_dataset, test_labels = reformat(test_dataset, test_labels)
print('Training set', train_dataset.shape, train_labels.shape)
print('Validation set', valid_dataset.shape, valid_labels.shape)
print('Test set', test_dataset.shape, test_labels.shape)

# Tensorflow Learning proces

train_subset = 10000
graph=tf.Graph()

with graph.as_default():
    # Size=10000*784
    tf_train_set = tf.constant(train_dataset[:train_subset, :])
    # Size=10000*10
    tf_train_label = tf.constant(train_labels[:train_subset, :])
    # Size=10000*784
    tf_valid_set = tf.constant(valid_dataset)
    # Size=10000*784
    tf_test_set = tf.constant(test_dataset)

    weight = tf.Variable(tf.truncated_normal([image_size * image_size, num_labels]))
    bias = tf.Variable(tf.zeros([num_labels]))

    # define the model as a linear model as y=W*x+B. logit is the calculated output of samples
    logit = tf.add(tf.matmul(tf_train_set, weight), bias)
    # act softmax function on the logit to normalize the output: e^(x_i)/sum(e^(x_n))
    # calculate the cross-entropy of the output: -label*log(output)' where label is a distribution or a one-hot encoding
    loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_label, logits=logit))

    optimizer = tf.train.GradientDescentOptimizer(0.5).minimize(loss)

    train_prediction = tf.nn.softmax(logit)
    valid_prediction = tf.nn.softmax(tf.matmul(tf_valid_set, weight) + bias)
    test_prediction = tf.nn.softmax(tf.matmul(tf_test_set, weight) + bias)


num_steps = 801


def accuracy(predictions, labels):
    return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
            / predictions.shape[0])


with tf.Session(graph=graph) as session:
    # This is a one-time operation which ensures the parameters get initialized as
    # we described in the graph: random weights for the matrix, zeros for the
    # biases.
    tf.global_variables_initializer().run()
    print('Initialized')
    for step in range(num_steps):
        # Run the computations. We tell .run() that we want to run the optimizer,
        # and get the loss value and the training predictions returned as numpy
        # arrays.
        _, l, predictions = session.run([optimizer, loss, train_prediction])
        if (step % 100 == 0):
            print('Loss at step %d: %f' % (step, l))
            print('Training accuracy: %.1f%%' % accuracy(
                predictions, train_labels[:train_subset, :]))
            # Calling .eval() on valid_prediction is basically like calling run(), but
            # just to get that one numpy array. Note that it recomputes all its graph
            # dependencies.
            print('Validation accuracy: %.1f%%' % accuracy(
                valid_prediction.eval(), valid_labels))
    print('Test accuracy: %.1f%%' % accuracy(test_prediction.eval(), test_labels))
