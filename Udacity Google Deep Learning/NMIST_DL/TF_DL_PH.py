# -*- coding:utf-8 -*-

"""
 @author: Jay
 @file: TF_DL_PH.py
 @time: 2017/12/19 23:47
"""
from __future__ import print_function
import numpy as np
import tensorflow as tf
from six.moves import cPickle as pickle
from six.moves import range

# Problem 7
# Use tensorflow to achieve the learning process

image_size = 28
num_labels = 10
pickle_file = 'notMNIST.pickle'

function_switch = 4

with open(pickle_file, 'rb') as f:
    save = pickle.load(f)
    train_dataset = save['train_dataset']
    train_labels = save['train_labels']
    valid_dataset = save['valid_dataset']
    valid_labels = save['valid_labels']
    test_dataset = save['test_dataset']
    test_labels = save['test_labels']
    del save  # hint to help gc free up memory
    print('Training set', train_dataset.shape, train_labels.shape)
    print('Validation set', valid_dataset.shape, valid_labels.shape)
    print('Test set', test_dataset.shape, test_labels.shape)

# Tensorflow Learning proces

graph = tf.Graph()


def accuracy(predictions, labels):
    return (100.0 * np.sum(np.argmax(predictions, 1) == np.argmax(labels, 1))
            / predictions.shape[0])


# Two-layer network
if function_switch == 0:
    # sk-learn method
    pass
elif function_switch == 1:
    # tensorflow gradient descent
    pass
elif function_switch == 2:
    # tensorflow SGD
    pass
elif function_switch == 3:
    # Reformat the data and labels
    def reformat(dataset, labels):
        dataset = dataset.reshape((-1, image_size * image_size)).astype(np.float32)
        # Map 0 to [1.0, 0.0, 0.0 ...], 1 to [0.0, 1.0, 0.0 ...]
        labels = (np.arange(num_labels) == labels[:, None]).astype(np.float32)
        return dataset, labels


    train_dataset, train_labels = reformat(train_dataset, train_labels)
    valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
    test_dataset, test_labels = reformat(test_dataset, test_labels)
    print('Training set', train_dataset.shape, train_labels.shape)
    print('Validation set', valid_dataset.shape, valid_labels.shape)
    print('Test set', test_dataset.shape, test_labels.shape)

    # two layer network
    batch_size = 256
    node_size = 1024
    with graph.as_default():
        tf_train_set = tf.placeholder(dtype=tf.float32, shape=[batch_size, image_size * image_size])
        tf_train_label = tf.placeholder(dtype=tf.float32, shape=[batch_size, num_labels])
        tf_valid_set = tf.constant(valid_dataset)
        tf_test_set = tf.constant(test_dataset)

        # the model is defined as 128*784 input -> 784 * 1024 weight + 1024 bias -> ReLU ->1024*10 weight+10 bias

        l1_weight = tf.Variable(tf.truncated_normal([image_size * image_size, node_size]))
        l1_bias = tf.Variable(tf.zeros(node_size))

        l2_weight = tf.Variable(tf.truncated_normal([node_size, node_size]))
        l2_bias = tf.Variable(tf.zeros(node_size))

        l3_weight = tf.Variable(tf.truncated_normal([node_size, node_size]))
        l3_bias = tf.Variable(tf.zeros(node_size))

        l4_weight = tf.Variable(tf.truncated_normal([node_size, node_size]))
        l4_bias = tf.Variable(tf.zeros(node_size))

        l5_weight = tf.Variable(tf.truncated_normal([node_size, num_labels]))
        l5_bias = tf.Variable(tf.zeros(num_labels))

        l1_logit = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(tf_train_set, l1_weight), l1_bias)), keep_prob=1)
        l2_logit = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(l1_logit, l2_weight), l2_bias)), keep_prob=1)
        l3_logit = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(l2_logit, l3_weight), l3_bias)), keep_prob=1)
        l4_logit = tf.nn.dropout(tf.nn.relu(tf.add(tf.matmul(l3_logit, l4_weight), l4_bias)), keep_prob=1)
        l5_logit = tf.add(tf.matmul(l4_logit, l5_weight), l5_bias) + 0.1 * tf.nn.l2_loss(
            l1_weight) + 0.1 * tf.nn.l2_loss(l2_weight) + 0.1 * tf.nn.l2_loss(l3_weight) + 0.1 * tf.nn.l2_loss(
            l4_weight) + + 0.1 * tf.nn.l2_loss(l5_weight)

        loss = tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_label, logits=l5_logit)
        start_learning_rate = 0.5
        global_rate = tf.Variable(0)
        learning_rate = tf.train.exponential_decay(start_learning_rate, global_rate, 1000, 0.96)
        optimizer = tf.train.AdadeltaOptimizer(learning_rate).minimize(loss)

        train_prediction = tf.nn.softmax(l5_logit)

        l1_valid_logit = tf.nn.relu(tf.add(tf.matmul(tf_valid_set, l1_weight), l1_bias))
        l2_valid_logit = tf.nn.relu(tf.add(tf.matmul(l1_valid_logit, l2_weight), l2_bias))
        l3_valid_logit = tf.nn.relu(tf.add(tf.matmul(l2_valid_logit, l3_weight), l3_bias))
        l4_valid_logit = tf.nn.relu(tf.add(tf.matmul(l3_valid_logit, l4_weight), l4_bias))
        valid_prediction = tf.nn.softmax(tf.add(tf.matmul(l4_valid_logit, l5_weight), l5_bias))

        l1_test_logit = tf.nn.relu(tf.add(tf.matmul(tf_test_set, l1_weight), l1_bias))
        l2_test_logit = tf.nn.relu(tf.add(tf.matmul(l1_test_logit, l2_weight), l2_bias))
        l3_test_logit = tf.nn.relu(tf.add(tf.matmul(l2_test_logit, l3_weight), l3_bias))
        l4_test_logit = tf.nn.relu(tf.add(tf.matmul(l3_test_logit, l4_weight), l4_bias))
        test_prediction = tf.nn.softmax(tf.add(tf.matmul(l4_test_logit, l5_weight), l5_bias))

    num_steps = 1000001
    with tf.Session(graph=graph) as sess:
        sess.run(tf.global_variables_initializer())
        for step in range(num_steps):
            offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
            batch_data = train_dataset[offset:(offset + batch_size), :]
            batch_label = train_labels[offset:(offset + batch_size), :]
            _, l, prediction = sess.run([optimizer, loss, train_prediction],
                                        feed_dict={tf_train_set: batch_data, tf_train_label: batch_label})
            if (step % 1000 == 0):
                # print(l)
                print('Training accuracy: %.1f%%' % accuracy(
                    prediction, batch_label))
                # Calling .eval() on valid_prediction is basically like calling run(), but
                # just to get that one numpy array. Note that it recomputes all its graph
                # dependencies.
                print('Validation accuracy: %.1f%%' % accuracy(
                    valid_prediction.eval(), valid_labels))
                print('Test accuracy: %.1f%%' % accuracy(test_prediction.eval(), test_labels))
                # print(prediction[0])
                # print(batch_label[0])
elif function_switch == 4:

    number_channels = 1
    batch_size = 16

    patch_size = 5
    depth = 16
    num_hidden = 64


    def reformat(dataset, labels):
        dataset = dataset.reshape(
            (-1, image_size, image_size, number_channels)).astype(np.float32)
        labels = (np.arange(num_labels) == labels[:, None]).astype(np.float32)
        return dataset, labels


    train_dataset, train_labels = reformat(train_dataset, train_labels)
    valid_dataset, valid_labels = reformat(valid_dataset, valid_labels)
    test_dataset, test_labels = reformat(test_dataset, test_labels)
    print('Training set', train_dataset.shape, train_labels.shape)
    print('Validation set', valid_dataset.shape, valid_labels.shape)
    print('Test set', test_dataset.shape, test_labels.shape)

    with graph.as_default():
        # define input
        tf_train_set = tf.placeholder(dtype=tf.float32, shape=[batch_size, image_size, image_size, number_channels])
        tf_train_label = tf.placeholder(dtype=tf.float32, shape=[batch_size, num_labels])
        tf_valid_set = tf.constant(valid_dataset)
        tf_test_set = tf.constant(test_dataset)

        # Variables.
        layer1_weights = tf.Variable(tf.truncated_normal(
            [patch_size, patch_size, number_channels, depth], stddev=0.1))
        layer1_biases = tf.Variable(tf.zeros([depth]))
        layer2_weights = tf.Variable(tf.truncated_normal(
            [patch_size, patch_size, depth, depth], stddev=0.1))
        layer2_biases = tf.Variable(tf.constant(1.0, shape=[depth]))
        layer3_weights = tf.Variable(tf.truncated_normal(
            [image_size//4 * image_size//4 * depth, num_hidden], stddev=0.1))
        layer3_biases = tf.Variable(tf.constant(1.0, shape=[num_hidden]))
        layer4_weights = tf.Variable(tf.truncated_normal(
            [num_hidden, num_labels], stddev=0.1))
        layer4_biases = tf.Variable(tf.constant(1.0, shape=[num_labels]))


        # Model.
        def model(data):
            conv = tf.nn.conv2d(data, layer1_weights, [1, 1, 1, 1], padding='SAME')
            conv_MP = tf.nn.max_pool(conv, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
            hidden = tf.nn.relu(conv_MP + layer1_biases)
            conv = tf.nn.conv2d(hidden, layer2_weights, [1, 1, 1, 1], padding='SAME')
            conv_MP = tf.nn.max_pool(conv, [1, 2, 2, 1], [1, 2, 2, 1], padding='SAME')
            hidden = tf.nn.relu(conv_MP + layer2_biases)
            shape = hidden.get_shape().as_list()
            reshape = tf.reshape(hidden, [shape[0], shape[1] * shape[2] * shape[3]])
            hidden = tf.nn.dropout(tf.nn.relu(tf.matmul(reshape, layer3_weights) + layer3_biases),keep_prob=1)
            return tf.matmul(hidden, layer4_weights) + layer4_biases


        logits = model(tf_train_set)
        loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(labels=tf_train_label, logits=logits))

        learning_ini=0.5
        global_rate=tf.Variable(0)
        learning_rate=tf.train.exponential_decay(learning_ini,global_rate,1000,0.95)
        optimizer = tf.train.AdadeltaOptimizer(learning_rate).minimize(loss)
        train_prediction = tf.nn.softmax(logits)
        valid_prediction = tf.nn.softmax(model(tf_valid_set))
        test_prediction = tf.nn.softmax(model(tf_test_set))

    num_steps = 10001

    with tf.Session(graph=graph) as session:
        tf.global_variables_initializer().run()
        print('Initialized')
        for step in range(num_steps):
            offset = (step * batch_size) % (train_labels.shape[0] - batch_size)
            batch_data = train_dataset[offset:(offset + batch_size), :, :, :]
            batch_labels = train_labels[offset:(offset + batch_size), :]
            feed_dict = {tf_train_set: batch_data, tf_train_label: batch_labels}
            _, l, predictions = session.run(
                [optimizer, loss, train_prediction], feed_dict=feed_dict)
            if (step % 50 == 0):
                # print('Minibatch loss at step %d: %f' % (step, l))
                print('Minibatch accuracy: %.1f%%' % accuracy(predictions, batch_labels))
                print('Validation accuracy: %.1f%%' % accuracy(
                    valid_prediction.eval(), valid_labels))
                print('Test accuracy: %.1f%%' % accuracy(test_prediction.eval(), test_labels))
