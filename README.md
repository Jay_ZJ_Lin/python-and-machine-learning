﻿# README #

This README would normally document whatever steps are necessary to get your application up and running.

### Source code Structure ###
* Kaggle Projects
	1. Titanic
* Python Projects:
	1. digit_robot_screener: 
		generate graphical random digit
	2. grapper
		grap pictures from internet
	3. number_indicator:
		generate numbers on the right-upper coner of a picture	
	4. word_count
		count word, characters, etc in a give text
* Udacity Google Deep Learning
	1. NMIST_DL
	2. Text_RNN
	3. LSTM
	4. Cam_Digit_Rec: final project
	
### Environment ###

JetBrains PyCharm Community Edition

Anaconda

### Reference and Resources ###

* Google-Deep-Learning Udacity:	https://classroom.udacity.com/courses/ud730

* Kaggle-Competitions:
	https://www.kaggle.com/competitions
	
* Stanford CS231n: Convolutional Neural Networks for Visual Recognition:
	http://cs231n.stanford.edu/
	http://cs231n.github.io/
	
* Stanford CS224d: Deep Learning for Natural Language Processing:
	http://cs224d.stanford.edu/

### Who do I talk to? ###
:Author： Zhongjie Lin

:Email: zhongjie.lin@mail.utoronto.ca